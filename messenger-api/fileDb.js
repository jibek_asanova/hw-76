const fs = require('fs');
const {nanoid} = require('nanoid');

const filename = './db.json';
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    if(data.length < 30) {
      return data;
    } else {
      return data.slice(data.length - 30);
    }
  },

  getItem(dateTime) {
    return data.find(i => i.dateTime === dateTime);
  },
  addItem(item) {
    item.id = nanoid();
    item.dateTime = (new Date()).toISOString();
    data.push(item);
    this.save();
    return item;
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data));
  }
};