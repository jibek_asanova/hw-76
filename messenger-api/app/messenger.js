const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
  const messages = fileDb.getItems();
  if(req.query.dateTime) {
    const date = new Date(req.query.dateTime);
    if(isNaN(date.getDate())) {
      return res.status(404).send({error: 'not valid data'})
    } else {
      const newArray = messages.filter(message => message.dateTime > req.query.dateTime);
      res.send(newArray);
    }
  } else {
    res.send(messages);
  }
});

router.get('/:dateTime', (req, res) => {
  const message = fileDb.getItem(req.params.dateTime);
  if(!message) {
    return res.status(404).send({error: 'Message not found'})
  }
  res.send(message);
});

router.post('/', (req, res) => {
  if(!req.body.message || !req.body.author) {
    return res.status(400).send({error: 'Author and message must be present in the request'})
  }
  const newMessage = fileDb.addItem({
    message: req.body.message,
    author: req.body.author,
  })
  res.send(newMessage);
});

module.exports = router;