import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Message from "../components/Message/Message";
import {createMessage, getAllMessages, getLast30Messages} from "../store/messagesActions";
import Form from "../components/Form/Form";
import './Messenger.css';
const Messenger = () => {
    const dispatch = useDispatch();
    const lastDate = useSelector(state => state.messages.date);
    const messages = useSelector(state => state.messages.messages);


    useEffect(() => {
        dispatch(getAllMessages());
    }, [dispatch]);

    const createNewMessage = async messageData => {
        await dispatch(createMessage(messageData));
    };

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(getLast30Messages(lastDate));
        },2000);

        return () => clearInterval(interval);
    }, [dispatch, lastDate]);


    return (
        <div>
            <div className="Messenger">
                <Form
                    sendMessage={createNewMessage}
                />
                <div className="MessageBlock">
                    <Message
                        messages={messages}
                    />
                </div>
            </div>
        </div>
    );
};

export default Messenger;