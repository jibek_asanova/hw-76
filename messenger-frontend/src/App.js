import Messenger from "./container/Messenger";

const App = () => (
    <div>
      <Messenger/>
    </div>
);

export default App;
