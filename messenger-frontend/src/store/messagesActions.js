import axiosApi from "../axiosApi";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const FETCH_LAST_30_MESSAGES_REQUEST = 'FETCH_LAST_30_MESSAGES_REQUEST';
export const FETCH_LAST_30_MESSAGES_SUCCESS = 'FETCH_LAST_30_MESSAGES_SUCCESS';
export const FETCH_LAST_30_MESSAGES_FAILURE = 'FETCH_LAST_30_MESSAGES_FAILURE';

export const CREATE_ADD_REQUEST = 'CREATE_ADD_REQUEST';
export const CREATE_ADD_SUCCESS = 'CREATE_ADD_SUCCESS';
export const CREATE_ADD_FAILURE = 'CREATE_ADD_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const fetchLast30MessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchLast30MessagesSuccess = messages => ({type: FETCH_LAST_30_MESSAGES_SUCCESS, payload: messages});
export const fetchLast30MessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const createAddRequest = () => ({type: CREATE_ADD_REQUEST});
export const createAddSuccess = () => ({type: CREATE_ADD_SUCCESS});
export const createAddFailure = () => ({type: CREATE_ADD_FAILURE});

export const getAllMessages = () => {
    return async dispatch => {
        try{
            dispatch(fetchMessagesRequest());
            const response = await axiosApi.get('/messages');
            dispatch(fetchMessagesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMessagesFailure());
        }
    }
};

export const getLast30Messages = (lastMessage) => {
    return async dispatch => {
        try {
            dispatch(fetchLast30MessagesRequest());
            const response = await axiosApi.get('/messages?dateTime=' + lastMessage);
            dispatch(fetchLast30MessagesSuccess(response.data));
        } catch (e) {
            dispatch(fetchLast30MessagesFailure());
        }
    }
};

export const createMessage = messageData => {
    return async dispatch => {
        try {
            dispatch(createAddRequest());
            await axiosApi.post('/messages', messageData);
            dispatch(createAddSuccess());
        } catch (e) {
            dispatch(createAddFailure());
        }
    }
}

