import {
    FETCH_LAST_30_MESSAGES_FAILURE,
    FETCH_LAST_30_MESSAGES_REQUEST, FETCH_LAST_30_MESSAGES_SUCCESS,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS
} from "./messagesActions";

const initialState = {
    fetchLoading: false,
    messages: [],
    date: null
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST:
            return {...state, fetchLoading: true}
        case FETCH_MESSAGES_SUCCESS:
            return {...state, fetchLoading: false, messages: action.payload, date: action.payload[action.payload.length - 1].dateTime}
        case FETCH_MESSAGES_FAILURE:
            return {...state, fetchLoading: false}
        case FETCH_LAST_30_MESSAGES_REQUEST:
            return {...state, fetchLoading: true}
        case FETCH_LAST_30_MESSAGES_SUCCESS:
            return {
                ...state,
                fetchLoading: false,
                messages: [...state.messages, ...action.payload],
                date: action.payload.length !== 0 ? action.payload[action.payload.length - 1].dateTime : state.date
            }
        case FETCH_LAST_30_MESSAGES_FAILURE:
            return {...state, fetchLoading: false}
        default:
            return state;
    }
}

export default messagesReducer;