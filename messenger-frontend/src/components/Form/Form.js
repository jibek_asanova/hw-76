import React, {useState} from 'react';
import './Form.css';

const Form = ({sendMessage}) => {
    const [state, setState] = useState({
        message: "",
        author: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        if(state.message === '' || state.author === '') {
            alert('please fill form');
            return;
        }
        sendMessage({...state});
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value}
        });
    };


    return (
        <form
            className="messageForm"
            onSubmit={submitFormHandler}
        >
            <div className="form-group">
                <label htmlFor="inputAuthor">Author:</label>
                <input
                    type="text"
                    onChange={inputChangeHandler}
                    name="author"
                    value={state.author}
                    className="form-control"
                    id="inputAuthor"
                />
            </div>
            <div className="form-group">
                <label htmlFor="inputMessage">Message:</label>
                <textarea
                    name="message"
                    onChange={inputChangeHandler}
                    value={state.message}
                    className="form-control"
                    id="inputMessage"
                    rows="3"
                />
            </div>
            <button type="submit" className="btn btn-primary" id="sendButton" onClick={sendMessage}>Send</button>
        </form>
    );
};

export default Form;